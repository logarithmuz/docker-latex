#!/bin/bash

echo '[multilib]' >> /etc/pacman.conf
echo 'Include = /etc/pacman.d/mirrorlist' >> /etc/pacman.conf
pacman -Sy --noconfirm --needed `cat /build/packages/pacman /build/packages/pacman-temp`
