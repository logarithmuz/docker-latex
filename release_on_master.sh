if [ $CI_BUILD_REF_NAME = "master" ]
then
  docker push $CI_REGISTRY_IMAGE:$CI_JOB_TIMESTAMP
  docker push $CI_REGISTRY_IMAGE:latest
else
  echo "Not running in master branch, not releasing new version"
fi
